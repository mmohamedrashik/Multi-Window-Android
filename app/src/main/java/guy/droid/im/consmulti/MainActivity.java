package guy.droid.im.consmulti;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView heading;
    RecyclerView recycler;
    Datapasser datapasser;
    ArrayList<String> heading_title;
    LinearLayoutManager horizontal_linear;
    LinearLayoutManager vertical_linear;
    SampleCards sample;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        heading = (TextView)findViewById(R.id.head);
        recycler = (RecyclerView)findViewById(R.id.recyclers);
        horizontal_linear = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        vertical_linear   = new LinearLayoutManager(this);
        recycler.setLayoutManager(vertical_linear);
        heading_title =  new ArrayList<>();
        for(int i = 0;i <= 20;i++){ heading_title.add("RAZ "+i); }


        datapasser = new Datapasser();
        datapasser.setHeading(heading_title);

        sample = new SampleCards(MainActivity.this,datapasser);

        recycler.setAdapter(sample);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if(isInMultiWindowMode())
            {
                Resize(true);
            }else
            {

                Resize(false);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if(isInMultiWindowMode())
            {
                Resize(true);
            }else
            {

                Resize(false);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onMultiWindowModeChanged(boolean isInMultiWindowMode) {
        super.onMultiWindowModeChanged(isInMultiWindowMode);
        Resize(isInMultiWindowMode);
    }


    public void Resize(Boolean window_type)
    {
        if(window_type ==  true)
        {
            heading.setTextSize(10);
            datapasser.multi_layout = true;
            sample.notifyDataSetChanged();
            recycler.setLayoutManager(horizontal_linear);
        }
        if(window_type ==  false){
            heading.setTextSize(18);
            datapasser.multi_layout = false;
            sample.notifyDataSetChanged();
            recycler.setLayoutManager(vertical_linear);

        }
    }
}
