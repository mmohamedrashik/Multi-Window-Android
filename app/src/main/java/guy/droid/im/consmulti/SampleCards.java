package guy.droid.im.consmulti;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by admin on 6/13/2017.
 */

public class SampleCards extends RecyclerView.Adapter<SampleCards.ViewHolder> {
    MainActivity activity;
    Datapasser datapasser;


    public SampleCards(MainActivity activity,Datapasser datapasser) {
        this.activity = activity;
        this.datapasser = datapasser;


    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cards,parent,false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(holder.textView.getTag(position) == "")
        {
            holder.textView.setTag(position,"POS"+position);

        }else
        {
            if(datapasser.multi_layout == false)
            {
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.linear.setLayoutParams(parms);

                parms.setMargins(0,0,0,10);
                holder.cardView.setLayoutParams(parms);



            }else {
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.linear.setLayoutParams(parms);
              //  holder.cardView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            }
            holder.textView.setText(datapasser.heading.get(position));
        }




    }

    @Override
    public int getItemCount() {
        return datapasser.heading.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        LinearLayout linear;
        CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);


            textView = (TextView)itemView.findViewById(R.id.child_head);
            linear = (LinearLayout)itemView.findViewById(R.id.main_linear);
            cardView = (CardView)itemView.findViewById(R.id.card_layout);



        }
    }
}